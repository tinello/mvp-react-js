/* global jest, describe, test, expect */
import HttpClient from '../core/infrastructure/HttpClient';

jest.mock('../core/infrastructure/HttpClient', () => {
  return jest.fn().mockImplementation(() => {
    return { post: () => { return {id: 101} }
    };
  });
});

const httpClient = new HttpClient();

describe('Pruebo cosas como: ', () => {

  test('1.- two plus two is four', () => {
    expect(2 + 2).toBe(4);
  });

  test('2.- two plus two is four', async () => {
    const r = await httpClient.post('', {id: 2});
    expect(r.id).toBe(101);
  });
});