/* global describe, it, expect, afterEach, afterAll, beforeAll, beforeEach */

//Common Matchers / Comparadores Comunes
describe('Comparadores comunes', () => {
  const user = {
      name: "Gustavo",
      lastname: "Tinello"
  };
  const user2 = {
      name: "Gabriel",
      lastname: "Tinello"
  };
  it('igualdad de elementos', () => {
      expect(user).toEqual(user);
  });
  it('No son exactamente iguales', () => {
      expect(user).not.toEqual(user2);
  });
});

//Numbers
const numbers = (a, b) => {
  return a + b;
}

describe('Comparacion de numeros', () => {
  it('Mayor que',  () => {
      expect(numbers(2,2)).not.toBeGreaterThan(9);
  });
  it('Mayor que',  () => {
    expect(numbers(2,2)).toBeGreaterThan(3);
  });
  it('Mayor que o igual', () =>{
      expect(numbers(2,2)).not.toBeGreaterThanOrEqual(30);
  });
  it('Mayor que o igual', () =>{
    expect(numbers(2,2)).toBeGreaterThanOrEqual(4);
  });
  it('Menor que', () => {
      expect(numbers(2,2)).not.toBeLessThan(1);
  });
  it('Menor que', () => {
    expect(numbers(2,2)).toBeLessThan(5);
  });
  it('Menor que o igual', () => {
      expect(numbers(2,2)).not.toBeLessThanOrEqual(1);
  });
  it('Numeros flotantes', () => {
      expect(numbers(0.2,0.2)).not.toBeCloseTo(0.5);
  });
});



//Truthness / Verdadero
const isNull = () => {
  return null
};
const isTrue = () => true;
const isFalse = () => false;
const isUndefined = () => undefined;

describe('Probar resultados nulos', () => {
  it('null', () => {
      expect(isNull()).toBeNull();
  });
});
describe('Probar resultados verdaderos', () => {
  it('Verdadero', () => {
      expect(isTrue()).toBeTruthy();
  });
});
describe('Probar resultados falsos', () => {
  it('falsos', () => {
      expect(isFalse()).toBeFalsy();
  });
});
describe('Probar resultados undefined', () => {
  it('undefined', () => {
      expect(isUndefined()).toBeUndefined();
  });
});


//Arrays
const fruits = ['banana', 'melon', 'sandia', 'naranja', 'limon', 'pepino'];
const colors = ['azul', 'verde', 'rojo', 'rosa', 'amarillo'];

const arrayFruits = () => fruits;
const arrayColors = () => colors;

describe('Comprobaremos que existe un elemento', () => {
  it('¿Tiene una banana?', () => {
      expect(arrayFruits()).toContain('banana');
  });
  it('No contiene un Platano', () => {
      expect(arrayFruits()).not.toContain('platano');
  });
  it('Comprobar el tamaño de un arreglo', () => {
      expect(arrayFruits()).toHaveLength(6);
  });
  it('Comprabaremos que existe un color', () => {
      expect(arrayColors()).toContain('azul');
  });
});


//Strings
describe('Comprobar cadenas de texto', () => {
  const text = 'un bonito texto';
  it('debe contener el siguiente texto', () => {
      expect(text).toMatch(/bonito/);
  });
  it('No contiene el siguiente texto', () => {
      expect(text).not.toMatch(/es/);
  });
  it('comprobar el tamaño de un texto', () => {
      expect(text).toHaveLength(15);
  });
});


//Setup de pruebas
// despues de cada prueba
afterEach(() => /*console.log('Despues de cada prueba')*/ {});
afterAll(() => /*console.log('Despues de todas las pruebas')*/ {});

// antes de cada prueba
beforeAll(() => /*console.log('Antes de todas las pruebas')*/ {});
beforeEach(() => /*console.log('Antes de cada prueba')*/ {});

describe('preparar antes de ejecutar', () => {
    it('Es verdadero', () => {
        expect(true).toBeTruthy();
    });
});


//Callbacks
function callbackHell(callback) {
  callback('Hola Javascripters');
}

describe('Probando un Callback', () => {
  it('Callback', done => {
      function otherCallback(data) {
          expect(data).toBe('Hola Javascripters')
          done();
      }
      callbackHell(otherCallback);
  });
});


//Promesas (DONE)
describe('Probando promesas', () => {
  it('Realizando una peticion a una api', done => {
      const api = 'https://rickandmortyapi.com/api/character/';
      fetch(api, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }}).then( async data => {
          const response = await data.json();
          expect(response.results.length).toBeGreaterThan(0);
          done();
      });
  });

  //Reject y resolve
  it('Resuelve un Hola!', () => {
    return expect(Promise.resolve('Hola!')).resolves.toBe('Hola!');
  });

  it('Rechaza con un error', () => {
    return expect(Promise.reject('Errror')).rejects.toBe('Errror');
});

});


//Async/Await
describe('Probar Async/Await', () => {
  it('Realizar una peticion a una api', () => {
    const api = 'https://rickandmortyapi.com/api/character/';
    const getRick = 'https://rickandmortyapi.com/api/character/1'

    fetch(api, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }}).then( async data => {
        const response = await data.json();
        expect(response.results.length).toBeGreaterThan(0);
    });

    fetch(getRick, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }}).then( async data => {
        const response = await data.json();
        expect(response.name).toEqual('Rick Sanchez');
    });
  });

  /*
  it('Realizando una peticion a una api con error', async () => {
    const apiError = 'http://httpstat.us/500';
    const peticion = getDataFromApi(apiError);
    await expect(peticion).rejects.toEqual(Error('Request failed with status code 500'));
  });
  */
 
  it('Resuelve un Hola', async () => {
    await expect(Promise.resolve('Hola')).resolves.toBe('Hola');
    await expect(Promise.reject('Error')).rejects.toBe('Error');
  });

});

