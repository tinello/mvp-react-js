export default class CreateCliente {
  constructor(httpClient) {
    this.httpClient = httpClient;
  }

  async execute(cliente) {
    return this.httpClient.post('https://jsonplaceholder.typicode.com/posts', cliente);
  }
}
