export default class HttpClient {
  async post(endpoint, body) {
    this.endpoint = endpoint;
    this.body = body;

    const optionalJsonBody = this.body ? JSON.stringify(this.body) : {};
    return fetch(this.endpoint, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: optionalJsonBody,
    }).then(async (r) => {
      const response = await r.json();
      if (r.status === 500 && response.message === 'InvalidAuthentication') {
        //console.log(500);
      }
      return response;
    });
  }
}
