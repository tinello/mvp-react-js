import HttpClient from './infrastructure/HttpClient';
import CreateCliente from './actions/CreateCliente';

const httpClient = new HttpClient();

const provider = {
  createCliente: new CreateCliente(httpClient),
};

export default provider;
