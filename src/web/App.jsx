import React from 'react';
import Provider from '../core/Provider';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: null,
      form: {
        codigo: '',
        firstName: '',
        lastName: '',
        email: '',
        telephone: '',
      } 
    };
  }

  handleClick = async e => {
    const provider = Provider;
    try {
      this.setState({loading: true, error: null});
      const result = await provider.createCliente.execute(this.state.form);
      this.setState({
        loading: false,
        error: null,
        form: {
            ...this.state.form,
            codigo: result.id,
        }
      });
    } catch (error) {
      this.setState({loading: false, error: error});
    }
  };

  handlerChange = e => {
    this.setState({
        form: {
            ...this.state.form,
            [e.target.name]: e.target.value,
        }
    });
  }

  render() {
    return (
      <div>
        <h1>Modelo Vista Presentador - React JS</h1>
        <h2>Registrar persona</h2>
        <div>
            <h3>Codigo:</h3>
            <input type="text" id="codigo" name="codigo" onChange={this.handlerChange} value={this.state.form.codigo} />
        </div>
        <div>
            <h3>Nombre:</h3>
            <input type="text" id="firstName" name="firstName" onChange={this.handlerChange} value={this.state.form.firstName} />
        </div>
        <div>
            <h3>Apellido:</h3>
            <input type="text" id="lastName" name="lastName" onChange={this.handlerChange} value={this.state.form.lastName} />
        </div>
        <div>
            <h3>Email:</h3>
            <input type="text" id="email" name="email" onChange={this.handlerChange} value={this.state.form.email} />
        </div>
        <div>
            <h3>Telefono:</h3>
            <input type="text" id="telephone" name="telephone" onChange={this.handlerChange} value={this.state.form.telephone} />
        </div>
        <div>
            <input type="button" value="Registrar" id="registerButton" onClick={this.handleClick} />
            {this.state.loading && (
              <span className="text-danger">Creando cliente!!!</span>
            )}
        </div>
        {this.state.error && (
          <p className="text-danger">{this.state.error.message}</p>
        )}
      </div>
    );
  }
}

export default App;
